<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'Administrator',
          'email' => 'admin@wallet.io',
          'is_admin' => 1,
          'api_token' => 'Zioj23D92j2kGf9D',
          'can_transact' => 1,
          'password' => bcrypt('@1234567'),
          'created_at' => date('Y-m-d H:i:s'),
      ]);

      DB::table('users')->insert([
          'name' => 'John Smith',
          'email' => 'john@wallet.io',
          'is_admin' => 0,
          'api_token' => str_random(60),
          'can_transact' => 1,
          'password' => bcrypt('@1234567'),
          'created_at' => date('Y-m-d H:i:s'),
      ]);
    }
}
