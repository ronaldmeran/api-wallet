<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // Define Schema
      Schema::create('transaction', function (Blueprint $table) {
          $table->increments('id_transaction');
          $table->string('transaction');
          $table->decimal('amount', 13, 2);
          $table->string('remarks');
          $table->decimal('old_balance', 13, 2);
          $table->decimal('new_balance', 13, 2);
          $table->integer('wallet_id')->unsigned();
          $table->foreign('wallet_id')->references('id_wallet')->on('wallet');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      // Define rollback
      Schema::dropIfExists('transaction');
    }
}
