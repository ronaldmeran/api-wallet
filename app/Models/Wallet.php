<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use SoftDeletes;

    /* string table name */
    protected $table = 'wallet';

    /* int primary key */
    protected $primaryKey = 'id_wallet';

    /* datetime deleted at */
    protected $dates = ['deleted_at'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	// Inherit from the parent class
    	parent::__construct();
    }

    /**
     * Display
     *
     * @param $where array
     * @param $orderby array
     * @param $count bool
     * @return $response array
     */
    public function display($where=array(),  $orderby = array('field' => array('id_wallet'), 'order' => 'ASC'), $count=FALSE)
    {
        // SELECT
        $query = self::_select();

        // JOIN
        $query = self::_join($query);

        // WHERE
        $query = self::_where($where, $query);

        // ORDER BY
        $query = self::_orderby($orderby, $query);

        if($count)
            return count( self::_get($query));

        // Response
        return self::_get($query);

    }

    /**
     * Select fields
     *
     * @param $query array
     * @return void
     */
    private function _select($query=array())
    {
        return $this->select('id_wallet', 'wallet_balance', 'users.name', 'users.email', 'wallet.created_at', 'wallet.updated_at');
    }

    /**
     * Join tables
     *
     * @param $query array
     * @return void
     */
    private function _join($query)
    {
        return $query
            ->leftJoin('users', 'users.id_user', '=', 'wallet.user_id');
    }

    /**
     * Filter fields
     *
     * @param $where array
     * @param $query array
     * @return void
     */
    private function _where($where=array(), $query=array())
    {
        return $query->where($where);
    }

    /**
     * Order by field in Ascending/Descending
     *
     * @param $orderby array
     * @param $query array
     * @return void
     */
    private function _orderby($orderby=array(), $query=array())
    {
        return $query->orderBy(implode('`, `', $orderby['field']), $orderby['order']);
    }

    /**
     * Get attributes
     *
     * @param $where array
     * @return $data array
     */
    private function _get($query=array())
    {
        $rs = $query->get();
        $data = array();

        foreach ($rs as $value)
            $data[] = (object) $value['attributes'];

        return $data;
    }

}
