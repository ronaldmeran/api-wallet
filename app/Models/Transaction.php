<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /* string table name */
    protected $table = 'transaction';

    /* int primary key */
    protected $primaryKey = 'id_transaction';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	// Inherit from the parent class
    	parent::__construct();
    }

    /**
     * Display
     *
     * @param $where array
     * @param $orderby array
     * @param $count bool
     * @return $response array
     */
    public function display($where=array(),  $orderby = array('field' => array('id_transaction'), 'order' => 'ASC'), $limit='', $count=FALSE)
    {
        // SELECT
        $query = self::_select();

        // JOIN
        $query = self::_join($query);

        // WHERE
        $query = self::_where($where, $query);

        // ORDER BY
        $query = self::_orderby($orderby, $query);

        // LIMIT
        $query = self::_limit($limit, $query);

        if($count)
            return count( self::_get($query));

        // Response
        return self::_get($query);

    }

    /**
     * Select fields
     *
     * @param $query array
     * @return void
     */
    private function _select($query=array())
    {
        return $this->select('id_transaction', 'transaction', 'amount', 'remarks', 'old_balance', 'new_balance',
                              'wallet_balance','transaction.created_at', 'transaction.updated_at');
    }

    /**
     * Join tables
     *
     * @param $query array
     * @return void
     */
    private function _join($query)
    {
        return $query
            ->leftJoin('wallet', 'wallet.id_wallet', '=', 'transaction.wallet_id');
    }

    /**
     * Filter fields
     *
     * @param $where array
     * @param $query array
     * @return void
     */
    private function _where($where=array(), $query=array())
    {
        return $query->where($where);
    }

    /**
     * Order by field in Ascending/Descending
     *
     * @param $orderby array
     * @param $query array
     * @return void
     */
    private function _orderby($orderby=array(), $query=array())
    {
        return $query->orderBy(implode('`, `', $orderby['field']), $orderby['order']);
    }

    /**
     * Filter fields
     *
     * @param $where array
     * @param $query array
     * @return void
     */
    private function _limit($limit='', $query=array())
    {
        return $query->take($limit);
    }

    /**
     * Get attributes
     *
     * @param $where array
     * @return $data array
     */
    private function _get($query=array())
    {
        $rs = $query->get();
        $data = array();

        foreach ($rs as $value)
            $data[] = (object) $value['attributes'];

        return $data;
    }

}
