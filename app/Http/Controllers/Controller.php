<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return the error message
     *
     * @param  str  $message
     * @return array
     */
    public function error_message($message)
    {
        // Return error message
        return array('error' => $message);
    }

    /**
     * Get the Admin API Token
     *
     * @return string $token
     */
    protected function adminToken()
    {
        // Get User Admin
        $user = User::where('is_admin', 1)->first();

        // Get token
        $token = $user->api_token;

        // Return API Token
        return trim($token);

    }
}
