<?php

namespace App\Http\Controllers\Wallet;

use Auth;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WalletController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson())
          return $this->error_message('Request must be a json object');

        // Check if Administrator
        if(trim($request->header('x-auth-key')) != $this->adminToken())
          return $this->error_message('Action cannot be performed. You must be an Administrator to add a wallet');

        // Validate
        self::validation($request);

        // Store Wallet
        $wallet_balance = $request->wallet_balance;

        // Find user for the corresponding email address
        $email = $request->email;
        $user = User::where('email', $email)->first();

        // Validate if user has already a wallet
        if(isset($user->id_user))
          $hasWallet = Wallet::where('user_id', $user->id_user)->first();

        if(isset($hasWallet))
          return $this->error_message('The user has already a wallet');

        if(empty($user))
          return $this->error_message('There is no user registered on that email address');

        $wallet = new Wallet;
        $wallet->user_id = $user->id_user;
        $wallet->wallet_balance = $wallet_balance;
        $wallet->save();

        return Wallet::find($wallet->id_wallet);

    }

    /**
     * Validate request
     *
     * @param Illuminate\Http\Request;
     * @return void
     */
    protected function validation(Request $request)
    {
        // Initialize
        $rules = array();
        $rules = array(
            'email' => 'required|email',
            'wallet_balance' => 'required|numeric',
        );

        if($request->segment(3) == 'delete'){
            $rules = array(
                'email' => 'required|email'
            );
        }

        $message = array(
            'email.required' => 'Email address is required',
            'email.email' => 'Email should be a valid email address',
            'wallet_balance.required' => 'Wallet Balance is required',
            'wallet_balance.numeric' => 'Wallet Balance must be a number',
        );

        // Validate
        $this->validate($request, $rules, $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!$request->isJson())
          return $this->error_message('Request must be a json object');

        // Check if Administrator
        if(trim($request->header('x-auth-key')) != $this->adminToken())
          return $this->error_message('Action cannot be performed. You must be an Administrator to delete a wallet');

        // Validate
        self::validation($request);

        // Find user for the corresponding email address
        $email = $request->email;
        $user = User::where('email', $email)->first();

        if(empty($user))
          return $this->error_message('There is no user registered on that email address');

        // Get wallet details
        $wallet = Wallet::where('user_id', $user->id_user);

        // Check if there is wallet to delete
        if(empty($wallet->first()))
          return $this->error_message('No wallet to delete');

        // Delete wallet
        if($wallet->delete())
          return $this->error_message('Wallet has been deleted');

        return $this->error_message('Select a wallet to delete');

    }

}
