<?php

namespace App\Http\Controllers\Transaction;

use Auth;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->isJson())
          return $this->error_message('Request must be a json object');

        // Validate
        self::validation($request);

        // Find user for the corresponding email address
        $email = $request->email;
        $user = User::where('email', $email)->first();

        if(empty($user))
          return $this->error_message('No user to update wallet');

        if(!$user->can_transact)
          return $this->error_message('Transaction Failed. Transaction cannot be completed');

        // Get the wallet id
        $wallet = Wallet::where('user_id', $user->id_user)->first();

        // Check if there is wallet to delete
        if(empty($wallet))
          return $this->error_message('No wallet to update');

        // Update Wallet
        $transaction = new Transaction;
        $transaction->transaction = $request->transaction;
        $transaction->wallet_id = $wallet->id_wallet;
        $transaction->amount = $request->amount;
        $transaction->remarks = $request->remarks;
        $transaction->old_balance = $wallet->wallet_balance; // Compute for old balance;

        if($request->transaction == 'reload')
        {
          $transaction->new_balance = $wallet->wallet_balance+$request->amount; // Compute for new balance;
        }
        elseif($request->transaction == 'deduct') {
          if($request->amount > $wallet->wallet_balance)
            return $this->error_message('Transaction Failed. Insufficient Wallet Balance');

          $transaction->new_balance = ($wallet->wallet_balance - $request->amount); // Compute for new balance;
        }
        else {
          return $this->error_message('Transaction type is invalid');
        }

        $transaction->save();

        // Update wallet
        Wallet::where('id_wallet', $wallet->id_wallet)->update(array('wallet_balance' => $transaction->new_balance));

        return Transaction::find($transaction->id_transaction);

    }

    /**
     * Validate request
     *
     * @param Illuminate\Http\Request;
     * @return void
     */
    protected function validation(Request $request)
    {
        // Initialize
        $rules = array();
        $rules = array(
            'transaction' => 'required',
            'amount' => 'required|numeric',
            'email' => 'required|email'
        );

        if($request->segment(3) == 'view'){
            $rules = array(
                'email' => 'required|email'
            );
        }

        $message = array(
            'transaction.required' => 'Transaction type is required',
            'amount.required' => 'Amount is required',
            'amount.numeric' => 'Amount must be a number',
            'email.required' => 'Email is required',
            'email.email' => 'Email must be a valid email address'
        );

        // Validate
        $this->validate($request, $rules, $message);

    }

    /**
     * Display the specified resource.
     *
     * @param Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // Check if json object
        if(!$request->isJson())
          return $this->error_message('Request must be a json object');

        // Validate
        self::validation($request);

        // Find user for the corresponding email address
        $email = $request->email;
        $user = User::where('email', $email)->first();

        if(empty($user))
            return $this->error_message('There is no user registered on that email address');

        // Get the wallet id
        $wallet = Wallet::where('user_id', $user->id_user)->first();

        if(empty($wallet))
          return $this->error_message('No data found');

        $transaction = new Transaction;
        $transaction = $transaction->display(
                  array('wallet_id' => $wallet->id_wallet),
                  array('field' => array('updated_at'), 'order' => 'DESC'), 3);

        if(empty($transaction))
          return $this->error_message('No data found');

        return $transaction;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
